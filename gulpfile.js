'use strict';

const gulp         = require('gulp'),                           // подключаем Gulp
    webserver      = require('browser-sync'),                   // сервер для работы и автоматического обновления страниц
    plumber        = require('gulp-plumber'),                   // модуль для отслеживания ошибок Gulp
    rigger         = require('gulp-rigger'),                    // модуль для импорта содержимого одного файла в другой
    sourcemaps     = require('gulp-sourcemaps'),                // модуль для генерации карты исходных файлов
    sass           = require('gulp-sass'),                      // модуль для компиляции SASS (SCSS) в CSS
    postcss        = require('gulp-postcss'),                   // postCss
    autoprefixer   = require('autoprefixer'),                   // модуль для автоматической установки автопрефиксов
    cleanCSS       = require('gulp-clean-css'),                 // плагин для минимизации CSS
    uglify         = require('gulp-uglify-es').default,         // модуль для минимизации JavaScript
    cache          = require('gulp-cache'),                     // модуль для кэширования
    imagemin       = require('gulp-imagemin'),                  // плагин для сжатия PNG, JPEG, GIF и SVG изображений
    webp           = require('imagemin-webp'),
    jpegrecompress = require('imagemin-jpeg-recompress'),       // плагин для сжатия jpeg
    pngquant       = require('imagemin-pngquant'),              // плагин для сжатия png
    rename         = require('gulp-rename'),
    htmlmin        = require('gulp-htmlmin'),
    htmlhint       = require("gulp-htmlhint"),
    del            = require('del'),                            // плагин для удаления файлов и каталогов
    pug            = require('gulp-pug'),
    stylus         = require('gulp-stylus'),
    stylelint      = require('gulp-stylelint'),
    cssnano        = require('cssnano'),
    postfocus      = require('postcss-focus'),
    postshort      = require('postcss-short'),
    lost           = require('lost'),
    mqpacker       = require("css-mqpacker"),
    cssnext        = require('postcss-cssnext'),
    concat         = require('gulp-concat'),
    ttf2woff       = require('gulp-ttf2woff'),
    ttf2woff2      = require('gulp-ttf2woff2'),
    ttf2svg        = require('gulp-ttf-svg'),
    ttf2eot        = require('gulp-ttf2eot'),
    consolidate    = require('gulp-consolidate'),
    iconfont       = require('gulp-iconfont'),
    gulpif         = require('gulp-if'),
    postcolor      = require('postcss-color-function'),
    extractMediaQuery = require('postcss-extract-media-query'),
    postcssCustomProperties = require('postcss-custom-properties'),
    csscomb        = require('gulp-csscomb');

var argv = require('yargs').argv;


var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

var isProduction = (argv.production !== undefined);

//=========================
// ICONS
//========================

gulp.task('fonts:icons', function () {
    return gulp.src('src/bundles/fonts/icons/**/*.svg')
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'iconfont',
            formats: ['woff', 'woff2'],
            appendCodepoints: true,
            appendUnicode: false,
            normalize: true,
            fontHeight: 1000,
            centerHorizontally: true
        }))
        .on('glyphs', function (glyphs, options) {
            gulpif(
                !isProduction,
                gulp.src('src/components/icon/_icon-template.scss')
                .pipe(consolidate('underscore', {
                    glyphs: glyphs,
                    fontName: options.fontName,
                    fontDate: new Date().getTime()
                }))
                .pipe(rename('_icon.scss'))
                .pipe(gulp.dest('src/components/icon'))
                .pipe(gulp.src('src/components/icon/_icon-template.html'))
                .pipe(consolidate('underscore', {
                    glyphs: glyphs,
                    fontName: options.fontName
                }))
                .pipe(rename('_icon.html'))
                .pipe(gulp.dest('src/components/icon'))
            );
        })
        .pipe(gulp.dest('dist/assets/fonts'));
});

//=========================
// FONTS
//========================

gulp.task('fonts:ttf2woff', function(){
    return gulp.src(['src/bundles/fonts/**/*.ttf'])
        .pipe(ttf2woff())
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:ttf2woff2', function(){
    return gulp.src(['src/bundles/fonts/**/*.ttf'])
        .pipe(ttf2woff2())
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:copy', function () {
    return gulp.src(['src/bundles/fonts/**/*.{woff2,woff}'])
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('fonts:convert', gulp.parallel('fonts:ttf2woff', 'fonts:ttf2woff2', 'fonts:copy'));

//=========================
// IMAGES
//========================

gulp.task('img:webp', () => {
    return gulp.src('src/bundles/images/**/*.{png,jpg,jpeg}')
        .pipe(plumber())
        .pipe(imagemin({
            verbose: true,
            plugins: webp({
                quality: 80,
                lossless: true
            })
        }))
        .pipe(rename((path) => { path.extname = ".webp" }))
        .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('img:compress', gulp.series('img:webp', () => {
    return gulp.src('src/bundles/images/**/*.{svg,gif,png,jpg,jpeg}')
        .pipe(plumber())
        .pipe(imagemin([
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            imagemin.gifsicle({interlaced: true}),
            imagemin.optipng(),
            pngquant({quality: [0.8, 0.9]}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest('dist/assets/images'))
}));

//gulp.task('img:convert', gulp.parallel('img:webp', 'img:compress'));
 gulp.task('img:convert', () => {
     return gulp.src('src/bundles/images/**/*')
         .pipe(gulp.dest('dist/assets/images'));
 });

//=========================
// JavaScript
//========================

gulp.task('js', () => {
    return gulp.src('src/bundles/main.js')
        .pipe(plumber())
        .pipe(rename({suffix: '.min'}))
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(gulpif(isProduction, uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist/assets/js'));
});

//=========================
// CSS
//========================

gulp.task('css:libs', () => {
        return gulp.src([
            // path tot lib in node_modules
        ])
        .pipe(concat('_libs.scss'));
    }
);

gulp.task('css:sass', gulp.series( () => {
    return gulp.src('src/bundles/**/!(_)*.{scss,sass}')
        .pipe(plumber())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'uncompressed'
        }))
        .pipe(postcss([
            lost(),
            mqpacker({sort : true}),
            postshort(),
            postcssCustomProperties(),
            autoprefixer({
                overrideBrowserslist: autoprefixerList
            }),
            /*cssnano({
                minifyFontWeight: false,
                calc: {precision: 4}
            })*/
        ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(webserver.reload({ stream:true }));
}));



gulp.task('html:pug', () => {
	return gulp.src('src/bundles/**/!(_)*.pug')
        .pipe(plumber())
        .pipe(pug({pretty : !isProduction}))
        .pipe(gulp.dest('dist'));
});

gulp.task('html:htmlhint', gulp.series ('html:pug', () => {
    return gulp.src('build/**/*.html')
        .pipe(plumber())
        .pipe(htmlhint('.htmlhintrc'))
        .pipe(htmlhint.reporter())
        .pipe(htmlhint.failAfterError())
        .pipe(gulpif(isProduction, htmlmin({
            collapseWhitespace: true,
            removeComments: true
        })))
        .pipe(gulp.dest('dist'))
}));

gulp.task('browser-sync', () => {
    webserver({
        server: {
            baseDir : 'dist/'
        },
        port : 3030
    });
});

gulp.task('watch:css', () => {
    gulp.watch('src/**/*.{scss,sass}', gulp.series('css:sass'));
});

gulp.task('watch:js', () => {
    gulp.watch('src/**/*.js', gulp.series('js', (done) => {
        webserver.reload();
        done();
    }));
});

gulp.task('watch:html', () => {
    gulp.watch('src/**/*.pug', gulp.series('html:pug', (done) => {
        webserver.reload();
        done();
    }));
});

gulp.task('watch', gulp.parallel('watch:css', 'watch:js', 'watch:html', 'browser-sync'));

gulp.task('build', gulp.series('img:convert', 'fonts:icons', 'fonts:convert','css:sass', 'js', 'html:pug'));

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'browser-sync')));