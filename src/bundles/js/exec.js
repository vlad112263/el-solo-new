$(document).ready(function () {
    $(this).scrollLeft(0);
    $(this).scrollTop(0);
});

$(document).ready(function(){
    $("a[data-behavior='smooth']").on('click', function(event) {
        if (this.hash !== "") {
            //event.preventDefault();
            let hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){
                window.location.hash = hash;
            });
        }
    });

    $('.js-example-basic-single').select2({
        templateSelection: formatState,
        allowClear: true
    });
});

function formatState (state) {
    let $state = $(
        '<span>Выбрано: <span></span></span>'
    );

    $state.find("span").text($(".select2-results__option--selected").length + 1);

    return $state;
};



/*SmoothScroll({
    // Время скролла 400 = 0.4 секунды
    animationTime    : 800,
    // Размер шага в пикселях
    stepSize         : 75,

    // Дополнительные настройки:

    // Ускорение
    accelerationDelta : 30,
    // Максимальное ускорение
    accelerationMax   : 2,

    // Поддержка клавиатуры
    keyboardSupport   : true,
    // Шаг скролла стрелками на клавиатуре в пикселях
    arrowScroll       : 50,

    // Pulse (less tweakable)
    // ratio of "tail" to "acceleration"
    pulseAlgorithm   : true,
    pulseScale       : 4,
    pulseNormalize   : 1,

    // Поддержка тачпада
    touchpadSupport   : true,
})*/

$(window).on('resize', function(){
    featureDots();
});
featureDots();

function featureDots() {
    $(".feature").each(function () {
        let name = $(this).find(".feature__name span");
        let value = $(this).find(".feature__value");

        if (name.height() / parseFloat(name.css('line-height')) > 1) {
            $(this).find(".feature__dots").css({bottom : '49.9%'})
        } else {
            $(this).find(".feature__dots").css({bottom : '4px'})
        }

        let width = $(this).width();

        $(this).find(".feature__dots").width(width - name.innerWidth() - value.innerWidth() - 16)

    });
}