/**
 * Создание DOM-элемента из строки
 * @param string HTML-код элемента
 * @returns {ChildNode}
 */
function createElemByString(string) {
    let template = document.createElement('template');
    template.innerHTML = string;
    return template.content.firstChild;
}

/**
 * Преобразует первый символ строки в верхний регистр
 * @param string Входная строка
 * @returns {string} Преобразованная срока
 */
function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function addListener(type, selector, listener, isCapture = false) {
    document.addEventListener(type, function (e) {
        if (e.target.classList.contains(selector)) {
            listener(e);
        }
    }, isCapture);
}