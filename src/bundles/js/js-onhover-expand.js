$(document).on('mouseover', ".js-onhover-expand", function (evnt) {
    let $el = $(this).find(".js-onhover-expand__element");

    $(this).addClass("js--hovered");
    $(this).css({zIndex : 1, height : $(this).closest(".card-wrapper").height() + $el.prop('scrollHeight') + "px"});

    $el.css({height : $el.prop('scrollHeight') + "px"});
});

$(document).on('mouseout', ".js-onhover-expand", function (evnt) {
    $(this).removeClass("js--hovered");
    $(this).css({zIndex : 0, height : $(this).closest(".card-wrapper").height() + "px"});

    let $el = $(this).find(".js-onhover-expand__element");
    $el.css({height : "0px"})
});


$(document).on('focus', ".js-onhover-expand", function (evnt) {
    $(this).addClass("js--hovered");
    $(this).css({zIndex : 1});

    let $el = $(this).find(".js-onhover-expand__element");
    $el.css({height : $el.prop('scrollHeight') + "px"})

    let $cards  = $(this).parents(".cards").find(".card");

    if ($cards.index($(this)) > 0) {
    }

});

$(document).on('blur', ".js-onhover-expand", function (evnt) {
    $(this).removeClass("js--hovered");
    $(this).css({zIndex : 0});

    let $el = $(this).find(".js-onhover-expand__element");
    $el.css({height : "0px"})
});