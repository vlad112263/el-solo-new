//= ../../node_modules/bootstrap/dist/js/bootstrap.bundle.js

//= js/utilities
//= js/js-onhover-expand

//= ../components/input/_input__field
//= ../components/alert/_alert
//= ../components/select/_select
//= ../components/dropdown/_dropdown
//= ../components/burger/_burger
//= ../components/tabs/_tabs
//= ../components/header/_header__search
//= ../components/rating-link/_rating-link
//= ../components/rating-stars/_rating-stars
//= ../components/menu/_menu__link
//= ../components/modal/_modal
//= ../components/drag-drop/_drag-drop
//= ../components/filter/_filter
//= ../components/aside/_aside
//= ../components/blind/_blind
//= js/exec

function card() {

    if ($(window).width() >= 1264) {
        $(".card-wrapper").each(function () {
            let el = $(this).find(".card");
            $(this).css({height: `${el.get(0).clientHeight}px`});
            el.css({position : 'absolute'});

            const info = $(this).find(".card__info");

            info.css({height: `${info.height()}px`});
            info.css({'flex-grow' : 0});

            $(this).find(".card__header").css({'flex-grow' : 0});
        });
    }

}

card();

$(window).resize(function () {
    card();
});


$('.modal [data-toggle="popover"]').popover({
    container: $('.modal'),
    html: true
});




$(document).ready(function () {

    var swiperImages = new Swiper(".landing-images", {
        slidesPerView: 2.05,
        spaceBetween: 10,
        containerModifierClass : 'landing-images',
        wrapperClass : 'landing-images-wrapper',
        slideClass : 'landing-image',
        slideToClickedSlide : false,
        breakpoints: {
            752: {
                slidesPerView: 3,
                centeredSlides: false,
                spaceBetween: 16,
            },
            1264 : {
                slidesPerView: 4,
                centeredSlides: false,
                spaceBetween: 16,
            }
        },
    });

    if ($(window).width() <= 1264) {
        var swiperFilter = new Swiper(".filter", {
            spaceBetween: 10,
            slidesPerView : 'auto',
            containerModifierClass : 'filter',
            wrapperClass : 'filter__list',
            slideClass : 'filter__item'
        });

        var swiperPayment = new Swiper(".payments", {
            slidesPerView : 2.5,
            containerModifierClass : 'payments',
            wrapperClass : 'payments-wrapper',
            slideClass : 'payment-slide',
            breakpoints: {
                752: {
                    slidesPerView: 2,
                    centeredSlides: false,
                    spaceBetween: 16,
                },
                1264 : {
                    slidesPerView: 3,
                    centeredSlides: false,
                    spaceBetween: 16,
                }
            },
        });
    }

    var swiperTopper = new Swiper(".topper-container", {
        slidesPerView : 'auto',
        containerModifierClass : 'topper-container',
        wrapperClass : 'topper-wrapper',
        slideClass : 'topper',
        centeredSlides: false,
        spaceBetween: 16,
    });

    var swiperCards = new Swiper(".cards", {
        slidesPerView: 1.05,
        spaceBetween: 16,
        containerModifierClass : 'cards',
        wrapperClass : 'cards-wrapper',
        slideClass : 'card-wrapper',
        keyboard: {
            enabled: true,
        },
        slideToClickedSlide : true,
        breakpoints: {
            752: {
                slidesPerView: 2,
                centeredSlides: false,
                spaceBetween: 16,
            },
            1264 : {
                slidesPerView: 3,
                centeredSlides: false,
                spaceBetween: 16,
            }
        },
    });
});

var swiper = new Swiper('.cards .swiper-container', {
    slidesPerView: 1.05,
    spaceBetween: 16,
   // centeredSlides: true,
    keyboard: {
        enabled: true,
    },
    slideToClickedSlide : true,
    breakpoints: {
        752: {
            slidesPerView: 2,
            centeredSlides: false,
            spaceBetween: 16,
        },
        1264 : {
            slidesPerView: 3,
            centeredSlides: false,
            spaceBetween: 16,
        }
    },
});


var swiperProduct = new Swiper('.product__gallery', {
    spaceBetween: 0,
    centeredSlides: true,
    pagination: {
        el: '.product__gallery-pagination',
    },
    navigation: {
        nextEl: '.product__gallery-button-next',
        prevEl: '.product__gallery-button-prev',
    },
});