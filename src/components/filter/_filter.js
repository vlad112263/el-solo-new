$(document).ready(function () {
    $(".filter").each(function () {
        if ($(window).width() > 1264) {
            const self = $(this);
            const width = $(this).width();
            const $items = $(this).find(".filter__item");
            const $specItem = $(this).find(".filter__more");

            let itemsWidth = $specItem.outerWidth(true);

            $items.each(function (index) {
                const w = $(this).outerWidth(true);
                itemsWidth += w;
                if (itemsWidth > width) {
                    $(this).hide();
                } else {
                    self.attr('data-limit', index);
                }
            });

            self.addClass("filter--collapse");
        }
    });
});

$(".filter__more").click(function () {
    const $filter = $(this).closest(".filter");
    const amount = $filter.data('limit');

    $(this).toggleClass("active");

    if ($filter.hasClass("filter--collapse")) {
        $filter.removeClass("filter--collapse");
        $filter.find(".filter__item").show();
    } else {
        $filter.addClass("filter--collapse");
        $filter.find(`.filter__item:gt(${amount})`).hide();
    }
});

$(".filter__link").click(function () {
    $(this).toggleClass("active");
});