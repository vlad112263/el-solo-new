$(".tabs").each(function () {
    let linePos   = 0;
    let lineWidth = 0;
    let $self   = $(this);
    let $active = $(this).find(".tabs__link--active");
    let $line   = $(this).find(".tabs__line");

    if ($active.length) {
        linePos   = $active.position().left;
        lineWidth = $active.width();

        $line.css({
            left:  linePos,
            width: lineWidth
        });
    }

    $(this).find(".tabs__link").click(function (evnt) {
        evnt.preventDefault();

        let $_this = $(this);

        if (
            ! $_this.hasClass("tabs__link--active") &&
            ! $self.hasClass("tabs--animate")
        ) {
            $self.addClass("tabs--animate");
            $self.find(".tabs__link").removeClass("tabs__link--active");

            let position = $_this.position();
            let width    = $_this.width();

            if(position.left >= linePos) {

                $line.animate({
                    width: ((position.left - linePos) + width)
                }, 300, function() {
                    $line.animate({
                        width: width,
                        left: position.left
                    }, 150, function() {
                        $self.removeClass('tabs--animate');
                    });
                    $_this.addClass('tabs__link--active');
                });
            } else {
                $line.animate({
                    left: position.left,
                    width: ((linePos - position.left) + lineWidth)
                }, 300, function() {
                    $line.animate({
                        width: width
                    }, 150, function() {
                        $self.removeClass('tabs--animate');
                    });
                    $_this.addClass('tabs__link--active');
                });
            }

            linePos = position.left;
            lineWidth= width;
        }
    })
});