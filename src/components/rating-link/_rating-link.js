$('.rating-link').click(function (event) {
    event.preventDefault();
    $(this).toggleClass("link--orangy");
    $(this).find(".burger").toggleClass("active");

    $(".page__body").toggleClass("overflowed");

    $(".category-section").toggleClass("show");
    $(".category-section-mobile").toggleClass("show");

    $("div.overflow").toggleClass("show");
    $("div.overflow").toggleClass("show-category-section");

    if ($(".category-section").hasClass("show")) {
        $(window).scrollTop(0);
    }
});

$(document).on('click', '.overflow.show-category-section', function () {
    $('.rating-link').trigger("click");
});

