$(".field").bind("input", function () {
    let placeholders = $(this).prevAll('.field__placeholder');
    if ($(this).val().length === 0) {
        placeholders.eq(0).show();
    } else {
        placeholders.eq(0).hide();
    }
});

$(".field").each(function () {
    let placeholders = $(this).prevAll('.field__placeholder');
    if ($(this).val().length === 0) {
        placeholders.eq(0).show();
    } else {
        placeholders.eq(0).hide();
    }
});