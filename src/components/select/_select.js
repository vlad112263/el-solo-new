window.onload = function () {
    document.querySelectorAll('.select').forEach((select) => {
        let options = select.querySelector('.select__list');
        options.style.maxHeight = `${options.clientHeight}px`;
    })
};

$(document).ready(function () {
    $(".select").each(function () {
        let options = $(this).find('.select__list');

        $(this).click(function (evnt) {
            $(this).toggleClass('select--opened');

            if ($(this).hasClass("select--opened")) {
                options.css({
                    height : options.prop('scrollHeight') + "px"
                });
            } else {
                options.css({
                    height : "0px"
                });
            }
        })
    });
});

$(window).click(function (event) {
    if (! event.target.closest('.select')) {
        $(".select--opened").removeClass("select--opened");
    }
});