$(document).ready(function () {
    $(".rating-stars--selectable").hover(
        function (event) {
            $(this).find(".rating-stars__item");
            let $rating = $(this).closest(".rating");
        },
        function (event) {
            const value = parseInt($(this).attr('data-value'));
            $(this).find(".rating-stars__item").removeClass('rating-stars__item--selected');
            $(this).find(`.rating-stars__item:lt(${value})`).addClass('rating-stars__item--selected');

            if (value === 0) {
                $(this).closest(".rating").find(".rating-value").remove();
            } else {
                const $rating = $(this).closest(".rating").find(".rating-value");
                $rating.text(value);
                $rating.removeClass().addClass(`rating-value rating-value--${value.toFixed(1)}`);
            }


        }
    );

    $(".rating-stars--selectable .rating-stars__item").hover(function () {
        let value = parseInt($(this).data('value'));
        const $parent = $(this).closest('.rating-stars--selectable');
        const $rating = $parent.closest(".rating");

        $parent.find(".rating-stars__item").removeClass('rating-stars__item--selected');
        $parent.find(`.rating-stars__item:lt(${value})`).addClass('rating-stars__item--selected');

        if ($rating.find(".rating-value").length === 0) {
            $rating.append(`<span class='rating-value rating-value--temporary'>${value}</span>`);
        } else {
            if ($rating.find(".rating-value").hasClass("rating-value--temporary")) {
                $rating.find(".rating-value").text(value);
            } else {
                $rating.find(".rating-value").text(value).removeClass().addClass(`rating-value rating-value--${value.toFixed(1)}`);
            }

        }

    });

    $(".rating-stars--selectable .rating-stars__item").click(function () {
        const value   = parseInt($(this).data('value'));
        const $parent = $(this).closest(".rating-stars--selectable");
        const $rating = $parent.closest(".rating");

        $parent.attr('data-value', value);
        $rating.find(".rating-value").text($(this).data('value')).removeClass().addClass(`rating-value rating-value--${value.toFixed(1)}`);
    });
});

